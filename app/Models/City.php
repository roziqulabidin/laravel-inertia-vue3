<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\State;

class City extends Model
{
    protected $table = 'kota';
    protected $primaryKey = 'id';
    protected $fillable = ['provinsi_id', 'namakota'];

    public function CityState(){
    	return $this->belongsTo(State::class, "provinsi_id");
    }
}
