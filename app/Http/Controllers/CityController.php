<?php

namespace App\Http\Controllers;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Models\City;
use App\Models\State;
use App\Http\Resources\CityCollection;
use App\Http\Resources\CityResource;
use App\Http\Requests\CityPostRequest;
use Inertia\Inertia;

class CityController extends Controller
{
    public function index()
    {
        $data = CityResource::collection(City::all()->load('CityState'));
        return Inertia::render('Kota/Index', 
        [
            'Index' => $data
        ]);
    }
    public function create()
    {
        $state = State::all();
        return Inertia::render('Kota/Create', 
        [
            'state' => $state
        ]);
    } 
    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(CityPostRequest $request)
    {
        $validated = $request->validated();

        $city = City::create([
            'provinsi_id'  => $request->provinsi_id,
            'namakota'     => $request->namakota,
        ]);
            return Redirect::route('cities.index')->with('message', 'Data Berhasil Disimpan!');
    }
    public function edit(City $city)
    {
        $state = State::all();
        return Inertia::render('Kota/Edit', [
            'city' => $city,
            'state' => $state
        ]);
    }
    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $post
     * @return void
     */
    public function update(CityPostRequest $request, City $city)
    {
        $validated = $request->validated();

        $city->update([
            'provinsi_id' => $request->provinsi_id,
            'namakota'    => $request->namakota
        ]);
            return Redirect::route('cities.index')->with('message', 'Data Berhasil Diupdate!');
    }
    public function destroy($id)
    {
        $city = City::findOrfail($id);
        $city->delete();
            return Redirect::route('cities.index')->with('message', 'Data Berhasil Dihapus!');
    }   
    public function StateList()
    {
        $data = City::get();
        return response()->json($data);
    }
    
}
