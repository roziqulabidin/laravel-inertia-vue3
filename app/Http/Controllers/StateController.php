<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Models\City;
use App\Models\State;
use App\Http\Resources\StateCollection;
use App\Http\Resources\StateResource;
use App\Http\Requests\StatePostRequest;

use Inertia\Inertia;


class StateController extends Controller
{
    public function index()
    {
        $data = StateResource::collection(State::all());
        return Inertia::render('Provinsi/Index', 
        [
            'Index' => $data
        ]);
    }
    public function create()
    {
        return Inertia::render('Provinsi/Create');
    } 
    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(StatePostRequest $request)
    {
        $validated = $request->validated();
        $state = State::create([
            'namaprovinsi' => $request->namaprovinsi,
        ]);
        return Redirect::route('states.index')->with('message', 'Data Berhasil Disimpan!');
    }
    public function edit(State $state)
    {
        return Inertia::render('Provinsi/Edit', [
            'state' => $state
        ]);
    }
    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $post
     * @return void
     */
    public function update(StatePostRequest $request, State $state)
    {
        $validated = $request->validated();
        
        $state->update([
            'namaprovinsi' => $request->namaprovinsi
        ]);
            return Redirect::route('states.index')->with('message', 'Data Berhasil Diupdate!');
    }
    public function destroy($id)
    {
        //find post by ID
        $state = State::findOrfail($id);

        //delete post
        $state->delete();

            return Redirect::route('states.index')->with('message', 'Data Berhasil Dihapus!');

    }
}
